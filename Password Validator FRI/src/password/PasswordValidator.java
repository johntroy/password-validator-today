package password;

/**
 * 
 * 
 * @author troya  991-530-754
 *Assume spacing are not valid characters for the purpose of calculating length
 *Assume null is not a valid character as a password
 *Assume characters should not be less than 8 characters.
 */

public class PasswordValidator {
	
    private static int MIN_LENGTH=8;
    
	public static boolean hasValidCaseChars(String password)
	{
		return  password!= null && password.matches(".*[A-Z]+.*")  && password.matches(".*[a-z]+.*")   ;
	}
	
	
     
	public static boolean isValidLength(String password) {
		return (password !=null && !password.contains(" ") && password.length() >=MIN_LENGTH);
	}
}
